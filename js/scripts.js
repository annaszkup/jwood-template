// Smooth scroll

$('#header-anchor').on('click', () => {
    document.querySelector('.header').scrollIntoView({
        behavior: 'smooth'
    });
})
$('#about-me-anchor').on('click', () => {
    document.querySelector('.about-me').scrollIntoView({
        behavior: 'smooth'
    });
})
$('#portfolio-anchor').on('click', () => {
    document.querySelector('.portfolio').scrollIntoView({
        behavior: 'smooth'
    });
})
$('#contact-anchor').on('click', () => {
    document.querySelector('.contact').scrollIntoView({
        behavior: 'smooth'
    });
})

// Changing menu color when reaches id position

if (($(window).width() >= 575) || $(window).resize()) {
    $(window).scroll(() => {
        var scroll = $(window).scrollTop();
        $('#nav').toggleClass('nav__reverse',
            scroll >= $('#about-me').offset().top
        );
    });
    $(window).scroll();
}
// Hamburger menu

$('#f-hamburger').on('click', () => {
    $(".nav__item").toggleClass("nav__list--open");
    $(".hamburger").toggleClass("hamburger__open");
});
