# JWood Template

A website based on JWood Template.

Technologies:
- HTML
- CSS
- JavaScript
- jQuery
- SASS
- Gulp

To download dependencies - npm install.

To compile use command "gulp".
